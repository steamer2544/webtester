function calculateDiscountedPrice(originalPrice, discountPercentage) {
  const discountAmount = originalPrice * (discountPercentage / 100);
  const discountedPrice = originalPrice - discountAmount;
  return discountedPrice;
}
module.exports = {
  calculateDiscountedPrice 
}

