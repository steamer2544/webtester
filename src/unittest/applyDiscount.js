const dis = require('./calculateDiscountedPrice');

function applyDiscount(product, discountPercentage) {
    const discountedPrice = dis.calculateDiscountedPrice(product.price, discountPercentage);
    product.discountedPrice = discountedPrice;
    return product;
  }
  
module.exports = {
    applyDiscount
}
  