const dis = require('./calculateDiscountedPrice');
const app = require('./applyDiscount');

describe('applyDiscount', () => {
    test('should apply the discount to the product', () => {
        const product = { name: 'Product 1', price: 100 };
        const discountPercentage = 20;
        const expectedDiscountedPrice = 80;

        const result = app.applyDiscount(product, discountPercentage);

        expect(result).toEqual({
        name: 'Product 1',
        price: 100,
        discountedPrice: expectedDiscountedPrice,
        });
    });
});